package net_utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class HostAdress {
	
	static Pattern ipv4Pattern = Pattern
			.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
	
	
	private static boolean validIP(String ip) {
		if (ip == null || ip.isEmpty())
			return false;
		ip = ip.trim();
		if ((ip.length() < 6) & (ip.length() > 15))
			return false;
		try {
			Matcher matcher = ipv4Pattern.matcher(ip);
			return matcher.matches();
		} catch (PatternSyntaxException ex) {
			return false;
		}
	}

	/*
	 * in case of many addresses
	 * gives the external address (not the dmz) if exists
	 * usefull when virtualbox has been installed on the station
	 */
	private static String findHostAddress() { // FP160906
		String result = null;
		List<String> addresses = new ArrayList<String>();
		try {
			Enumeration<NetworkInterface> n = NetworkInterface
					.getNetworkInterfaces();
			while (n.hasMoreElements()) {
				NetworkInterface e = n.nextElement();
				Enumeration<InetAddress> a = e.getInetAddresses();
				while (a.hasMoreElements()) {
					InetAddress addr = a.nextElement();
					String hname = addr.getHostName();
					if (hname.equals(addr.getCanonicalHostName())) {
						String hadress = addr.getHostAddress();
						if (validIP(hadress) && !hadress.equals("127.0.0.1"))
							addresses.add(hadress);
					}
				}
			}
		} catch (Exception e) {
		}
		if (addresses.size() >= 1) {
			result = addresses.get(0);
			for (String adr : addresses) {
				if (!adr.startsWith("192.") && !adr.startsWith("10.")) {
					result = adr;
					break;
				}
			}
		}
		return result;
	}

	
	//usage: String host=HostAdress.getHostAddress();
	public static String getHostAddress() {
		try {
			String host = InetAddress.getLocalHost().getHostAddress();
			String serverHost = findHostAddress();
			if (!host.equals(serverHost)) {
				System.out.println("multiple addresses: [" + serverHost
						+ "] vs " + host);
				host = serverHost;
			}
			return host;
		} catch (Exception e) {
			return null;
		}
	}

}
