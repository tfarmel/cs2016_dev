package cs_2016_tp1_server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

import net_utils.HostAdress;

public class CircleServer {

	private static final int PORT = 8051;

	public static void main(String[] args) {
		try {
			serveur();
		} catch (Exception e) {
			System.out.println(e.toString());
			// e.printStackTrace();
		}
	}

	public static void serveur() throws Exception {
		ServerSocket socketEcoute = new ServerSocket(PORT);
		String localhost = HostAdress.getHostAddress();
		System.out.println("[serveur monosession] d�marr� sur :" + localhost
				+ ":" + socketEcoute.getLocalPort());
		boolean endServer = false;
		while (!endServer) {
			System.out.println("en attente d'une connexion");
			Socket socketService = socketEcoute.accept(); // bloquant ici
			System.out.println("le client "
					+ socketService.getRemoteSocketAddress()
					+ " s'est connect�");
			PrintStream output = new PrintStream(
					socketService.getOutputStream(), true); // autoflush
			BufferedReader networkIn = new BufferedReader(
					new InputStreamReader(socketService.getInputStream()));
			while (true) {
				String requeteclient = networkIn.readLine();
				if (requeteclient == null) {
					System.out
							.println("le client s'est d�connect�, fin de la session");
					break;
				}
				System.out.println("le client demande: "
						+ (requeteclient == null ? "null" : requeteclient));
				if (requeteclient != null) {
					if (requeteclient.toLowerCase().contains("stop")) {
						// requ�te attendue: stop
						output.println("stopping session");
						break;
					} else if (requeteclient.toLowerCase().contains("end")) {
						// requ�te attendue: end
						output.println("end server");
						endServer = true;
						break;
					} else {
						String[] req = requeteclient.split(" ");
						if (req[0].toLowerCase().equals("date")) {
							// requ�te attendue: date
							Date now = new Date();
							output.println("il est " + now.toString());
						} else {
							// requ�te attendue: circle 10 15 50 - arguments: x
							// y
							// radius
							if (req[0].toLowerCase().equals("circle")) {
								try {
									String x = req[1];
									String y = req[2];
									String radius = req[3];
									String resp = "un cercle � x=" + x + " y="
											+ y + " rayon=" + radius;
									output.println(resp);
								} catch (Exception e) {
									output.println("erreur dans la commande "
											+ requeteclient
											+ " format attendu:" + "x y radius");
								}
							} else {
								output.println("commande inconnue ");
								output.println("Je connais les commandes suivantes:");
								output.println("date => je retourne la date");
								output.println("circle x y radius => je \"dessine\" un cercle de rayon radius � la coordonn�e x y");
								output.println("stop => fin de la session");
								output.println("end => fin du serveur");
							}
						}
					}
				}
			}
			socketService.close();
			System.out.println("arr�t de la session");
		}
		socketEcoute.close();
		System.out.println("arr�t du serveur");
	}

}